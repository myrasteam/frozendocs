![Photography by Dan Foley][ffist]

# Project FrozenFist
## VortexOps

> This project, codenamed "**FrozenFist**", is a software design project for semi-autonomous armored assault vehicles. The vehicles are designed to operate either remotely with a human pilot or in "robot mode" with guidance from an on-board Artificial Intelligence (AI) system.

Contact: [Devan Shepherd](mailto:Devan.Shepherd@rasmussen.edu )




[ffist]: http://i.huffpost.com/gen/980235/thumbs/o-FROZEN-FIST-OF-DETROIT-SCULPTURE-570.jpg?6 "CC: Photography by Dan Foley, published in Huffington Post - lic: Creative Commons"


### Pushed to BitBucket Repository - 15 January 2018 - 11 02 14
### Modified README on BitBucket - 16 February 2018 - 02 52 40